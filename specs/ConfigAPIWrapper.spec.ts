import { HostRules, Locales } from 'redirector-client'

import ConfigAPIWrapper from '../src/ConfigAPIWrapper'

import { randomArray } from './factories/ArrayFactory'
import { randomHostRules } from './factories/HostRulesFactory'
import { randomLocaleTranslations } from './factories/LocaleTranslationsFactory'
import { randomRule } from './factories/RuleFactory'
import { ConfigApiMock } from './mocks/ConfigApiMock'

let makeTestHostRule = () =>
  randomHostRules({
    rules: [
      randomRule(),
      randomRule({
        activeFrom: undefined,
      }),
      randomRule({
        activeTo: undefined,
      }),
      randomRule({
        activeTo: undefined,
        activeFrom: undefined,
      }),
    ],
  })

let makeDecodedHostRule = (hostRule: HostRules) =>
  JSON.parse(JSON.stringify(hostRule))

describe('ConfigAPIWrapper', () => {
  let configAPIMock: ConfigApiMock
  let configAPIWrapper: ConfigAPIWrapper

  beforeEach(() => {
    configAPIMock = new ConfigApiMock()
    configAPIWrapper = new ConfigAPIWrapper(configAPIMock)
  })

  describe('createHostRule', () => {
    describe('when resolved', () => {
      let hostRules: HostRules
      let decodedHostRule: HostRules

      beforeEach(() => {
        hostRules = makeTestHostRule()
        decodedHostRule = makeDecodedHostRule(hostRules)

        configAPIMock.createHostRules.mockResolvedValue(decodedHostRule)
      })

      it('return host rules', async () => {
        await expect(configAPIWrapper.createHostRules(hostRules)).resolves.toEqual(hostRules)
      })
    })
  })

  describe('deleteHostRule', () => {
    describe('when resolved', () => {
      let hostRules: HostRules

      beforeEach(() => {
        hostRules = makeTestHostRule()

        configAPIMock.deleteHostRules.mockResolvedValue({})
      })

      it('return host rules', async () => {
        await expect(configAPIWrapper.deleteHostRules(hostRules.host)).resolves.toEqual({})
      })
    })
  })

  describe('getHostRule', () => {
    describe('when resolved', () => {
      let hostRules: HostRules
      let decodedHostRule: HostRules

      beforeEach(() => {
        hostRules = makeTestHostRule()
        decodedHostRule = makeDecodedHostRule(hostRules)

        configAPIMock.getHostRule.mockResolvedValue(decodedHostRule)
      })

      it('return host rules', async () => {
        await expect(configAPIWrapper.getHostRule(hostRules.host)).resolves.toEqual(hostRules)
      })
    })
  })

  describe('listHostRules', () => {
    describe('when resolved', () => {
      let hostRules: HostRules
      let decodedHostRule: HostRules

      beforeEach(() => {
        hostRules = makeTestHostRule()
        decodedHostRule = makeDecodedHostRule(hostRules)

        configAPIMock.listHostRules.mockResolvedValue([decodedHostRule])
      })

      it('return list of host rules', async () => {
        await expect(configAPIWrapper.listHostRules()).resolves.toEqual([hostRules])
      })
    })
  })

  describe('locales()', () => {
    describe('when resolved', () => {
      let locales: Locales
      beforeEach(() => {
        locales = randomArray(randomLocaleTranslations, 1, 3)
        configAPIMock.locales.mockResolvedValue(locales)
      })

      it('return locales', async () => {
        await expect(configAPIWrapper.locales()).resolves.toEqual(locales)
      })
    })
  })

  describe('updateHostRule', () => {
    describe('when resolved', () => {
      let hostRules: HostRules
      let decodedHostRule: HostRules

      beforeEach(() => {
        hostRules = makeTestHostRule()
        decodedHostRule = makeDecodedHostRule(hostRules)

        configAPIMock.updateHostRules.mockResolvedValue(decodedHostRule)
      })

      it('return host rules', async () => {
        await expect(configAPIWrapper.updateHostRules(hostRules.host, hostRules))
          .resolves.toEqual(hostRules)
      })
    })
  })
})
