import Button from '@material-ui/core/Button'
import FormControl from '@material-ui/core/FormControl'
import TextField from '@material-ui/core/TextField'
import * as React from 'react'
import {
  HostRules,
  ModelValidationError,
  Rule,
  Target,
} from 'redirector-client'

import AppContext, {
  AppContextData,
} from '../AppContext'
import {
  buildLocalizer,
  findLocaleTranslations,
} from '../utils/localize'
import {
  embedValidationErrors,
  fieldValidationErrors,
} from '../utils/validationErrors'

import RuleForm, { MoveRule } from './RuleForm'
import TargetForm from './TargetForm'

export type UpdateHostRules = (hostRules: HostRules) => void

export interface HostRulesFormProps {
  hostRules: HostRules,
  onUpdateHostRules: UpdateHostRules,
  modelError: ModelValidationError,
}

export default class HostRulesForm extends React.Component<
  HostRulesFormProps
> {
  hostRef = React.createRef<HTMLInputElement>()

  render () {
    return <AppContext.Consumer>{this.renderWithContext}</AppContext.Consumer>
  }

  private renderWithContext = (appContext: AppContextData) => {
    let localeTranslations = findLocaleTranslations(appContext.errorLocales)
    let localize = buildLocalizer(localeTranslations)
    return (
      <>
        <TextField
          name='host'
          label='Host'
          value={this.props.hostRules.host}
          onChange={this.onInputChange}
          fullWidth
          required
          error={this.fieldErrors('host').length > 0}
          helperText={this.fieldErrors('host').map(localize).join(', ')}
          inputRef={this.hostRef}
        />

        <br />

        <h3>Default Target</h3>
        <TargetForm
          target={this.props.hostRules.defaultTarget}
          onUpdateTarget={this.updateTarget}
          modelError={embedValidationErrors(this.props.modelError, 'defaultTarget')}
        />

        <FormControl fullWidth>
          <h2>Rules</h2>
          {this.props.hostRules.rules.map(this.renderRuleForm)}

          <Button name='addRule' onClick={this.addRule}>
            Add
          </Button>
        </FormControl>
      </>
    )
  }

  componentDidMount () {
    this.hostRef.current!.focus()
  }

  private renderRuleForm = (rule: Rule, index: number) => (
    <RuleForm
      key={index}
      rule={rule}
      ruleIndex={index}
      onUpdateRule={this.updateRule(index)}
      onRemoveRule={this.removeRule(index)}
      onMoveUpRule={this.moveUpRule(index)}
      onMoveDownRule={this.moveDownRule(index)}
      modelError={embedValidationErrors(this.props.modelError, 'rules', index)}
    />
  )

  private onInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    event.preventDefault()
    const name = event.target.name
    const value = event.target.value
    this.props.onUpdateHostRules({
      ...this.props.hostRules,
      [name]: value,
    })
  }

  private updateTarget = (target: Target) => {
    this.props.onUpdateHostRules({
      ...this.props.hostRules,
      defaultTarget: target,
    })
  }

  private updateRule = (index: number) => (rule: Rule) => {
    const rules = [
      ...this.props.hostRules.rules.slice(0, index),
      rule,
      ...this.props.hostRules.rules.slice(index + 1),
    ]

    const hostRules: HostRules = {
      ...this.props.hostRules,
      rules,
    }

    this.props.onUpdateHostRules(hostRules)
  }

  private removeRule = (index: number) => () => {
    const newRules = [
      ...this.props.hostRules.rules.slice(0, index),
      ...this.props.hostRules.rules.slice(index + 1),
    ]

    this.props.onUpdateHostRules({
      ...this.props.hostRules,
      rules: newRules,
    })
  }

  private addRule = () => {
    const newRules = this.props.hostRules.rules.concat([{
      sourcePath: '',
      resolver: Rule.ResolverEnum.Simple,
      target: {
        httpCode: 301,
        path: '',
      },
    }])

    this.props.onUpdateHostRules({
      ...this.props.hostRules,
      rules: newRules,
    })
  }

  private moveUpRule = (index: number): MoveRule | undefined => {
    if (index === 0) {
      return undefined
    }

    return () => {
      const prevRule = this.props.hostRules.rules[index - 1]
      const rule = this.props.hostRules.rules[index]
      const newRules = [
        ...this.props.hostRules.rules.slice(0, index - 1),
        rule,
        prevRule,
        ...this.props.hostRules.rules.slice(index + 1),
      ]

      this.props.onUpdateHostRules({
        ...this.props.hostRules,
        rules: newRules,
      })
    }
  }

  private moveDownRule = (index: number): MoveRule | undefined => {
    if ((index + 1) === this.props.hostRules.rules.length) {
      return undefined
    }

    return () => {
      const rule = this.props.hostRules.rules[index]
      const nextRule = this.props.hostRules.rules[index + 1]
      const newRules = [
        ...this.props.hostRules.rules.slice(0, index),
        nextRule,
        rule,
        ...this.props.hostRules.rules.slice(index + 2),
      ]

      this.props.onUpdateHostRules({
        ...this.props.hostRules,
        rules: newRules,
      })
    }
  }

  private fieldErrors = (fieldName: string): Array<string> =>
    fieldValidationErrors(this.props.modelError, fieldName)
      .map(_ => _.translationKey)
}
