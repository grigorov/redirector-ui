import DayjsFnsUtils from '@date-io/dayjs'
import AppBar from '@material-ui/core/AppBar'
import Button from '@material-ui/core/Button'
import CssBaseline from '@material-ui/core/CssBaseline'
import Link from '@material-ui/core/Link'
import Toolbar from '@material-ui/core/Toolbar'
import withStyles, {
  WithStyles,
} from '@material-ui/core/styles/withStyles'
import { MuiPickersUtilsProvider } from '@material-ui/pickers'
import * as React from 'react'
import {
  Locales,
} from 'redirector-client'

import AppContext from './AppContext'
import Config, {
  ConfigApiBuilder,
  ConfigStore,
} from './Config'
import ErrorView from './components/ErrorView'
import Loader from './components/Loader'
import {
  AuthorizedRoutes,
  UnauthorizedRoutes,
} from './routes'

class AppState {
  errorLocales?: Locales
  errorLocalesError?: Error

  constructor (
    readonly config?: Config,
  ) {}
}

export interface AppProps {
  apiUrl?: string
  configApiBuilder: ConfigApiBuilder
  configStore: ConfigStore
  authorizedRoutes: AuthorizedRoutes
  unauthorizedRoutes: UnauthorizedRoutes
}

const styles =
  withStyles({
    toolBarTitle: {
      flex: 1,
    },
    wrapper: {
      height: '100%',
      display: 'flex',
      flexDirection: 'column',
    },
  })

class App extends React.Component<AppProps & WithStyles, AppState> {
  state = new AppState(this.props.configStore.load())

  render () {
    let classes = this.props.classes
    return (
      <div className={classes.wrapper}>
        <CssBaseline />

        <AppBar position='sticky' color='default'>
          <Toolbar>
            <Link
              href='/'
              title='Home'
              variant='h6'
              className={classes.toolBarTitle}
            >
              Redirector
            </Link>
            {this.logOutButton()}
          </Toolbar>
        </AppBar>

        <MuiPickersUtilsProvider utils={DayjsFnsUtils}>
          {this.routes()}
        </MuiPickersUtilsProvider>
      </div>
    )
  }

  componentDidMount () {
    if (undefined !== this.state.config) {
      this.loadErrorLocales(this.state.config)
    }
  }

  private logIn = (config: Config) => {
    this.setState({ config })
    this.props.configStore.store(config)
    this.loadErrorLocales(config)
  }

  private logOut = () => {
    this.setState({ config: undefined })
    this.props.configStore.clear()
  }

  private logOutButton () {
    if (undefined === this.state.config) return undefined
    return (
      <Button
        name='logOut'
        onClick={this.logOut}
      >
        Log out
      </Button>
    )
  }

  private routes (): JSX.Element {
    if (undefined === this.state.config) {
      return this.props.unauthorizedRoutes({
        apiUrl: this.props.apiUrl,
        logIn: this.logIn,
      })
    }

    if (undefined !== this.state.errorLocalesError) {
      return <ErrorView response={this.state.errorLocalesError} />
    }

    if (undefined === this.state.errorLocales) {
      return <Loader label='Load locales...' />
    }

    let configApi = this.props.configApiBuilder(this.state.config)
    return (
      <AppContext.Provider value={{ errorLocales: this.state.errorLocales }}>
        {this.props.authorizedRoutes({ configApi })}
      </AppContext.Provider>
    )
  }

  private loadErrorLocales (config: Config) {
    this.props.configApiBuilder(config)
      .locales()
      .then(this.setErrorLocales)
      .catch(this.setErrorLocaelsError)
  }

  private setErrorLocales = (locales: Locales) =>
    this.setState({
      errorLocales: locales,
    })

  private setErrorLocaelsError = (error: Error) =>
    this.setState({
      errorLocalesError: error,
    })
}

export default styles(App)
