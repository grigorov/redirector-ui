# Redirector UI

WEB UI for [redirector](https://gitlab.com/c0va23/redirector)

[![pipeline status](https://gitlab.com/c0va23/redirector-ui/badges/master/pipeline.svg)](https://gitlab.com/c0va23/redirector-ui/commits/master)
[![coverage report](https://gitlab.com/c0va23/redirector-ui/badges/master/coverage.svg)](https://gitlab.com/c0va23/redirector-ui/commits/master)
[![Docker Repository on Quay](https://quay.io/repository/c0va23/redirector-ui/status "Docker Repository on Quay")](https://quay.io/repository/c0va23/redirector-ui)


## Run

### Run with docker

```bash
docker run -p 8080:80 -e API_URL=http://myhost.org quay.io/c0va23/redirector-ui
```

When `API_URL` is url of redirector server. Can be skipped.

## Build

### Requires

Build tested with:

- nodejs (version 11.11.0)
- make (version 4.1)
- docker (version 18.09.3-ce)

```bash
make lint
make test

# Build assets
make dist/index.html
# Show result
ls dist/*

# Or build image
make build-docker-image
```
